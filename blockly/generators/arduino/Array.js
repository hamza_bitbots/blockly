goog.provide('Blockly.Arduino.Array');

goog.require('Blockly.Arduino');

Blockly.Arduino.Array_variable = function() {
  //var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000'
  var dropdown_direction2 =  this.getFieldValue("type");
  var text_var = this.getFieldValue('var_name');
  var value_arrayvar = Blockly.Arduino.valueToCode(this, 'item1', Blockly.Arduino.ORDER_NONE) || '0';
  var value_arrayvar1 = Blockly.Arduino.valueToCode(this, 'item2', Blockly.Arduino.ORDER_NONE)|| '0';
  var value_arrayvar2 = Blockly.Arduino.valueToCode(this, 'item3', Blockly.Arduino.ORDER_NONE)|| '0';
  var value_arrayvar3 = Blockly.Arduino.valueToCode(this, 'item4', Blockly.Arduino.ORDER_NONE)|| '0';
  var value_arrayvar4 = Blockly.Arduino.valueToCode(this, 'item5', Blockly.Arduino.ORDER_NONE) || '0';
  // TODO: Assemble JavaScript into code variable.
  Blockly.Arduino.definitions_['define_array'+ text_var] = ''+ dropdown_direction2 +''+' '+''+ text_var +'[5]'+'='+'{'+ value_arrayvar +',' + value_arrayvar1 +','+ value_arrayvar2+','+ value_arrayvar3+','+ value_arrayvar4+'};';
var code = '';
  return code;
  

};

Blockly.Arduino.Array_pointer= function() {
  //var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000'
  var dropdown_direction = this.getFieldValue('var_name') ;
  var dropdown_direction2 = this.getFieldValue('NAME2');
  var value_changedvar = Blockly.Arduino.valueToCode(this, 'change_element', Blockly.Arduino.ORDER_NONE);
  // TODO: Assemble JavaScript into code variable.
var code = ''+dropdown_direction+'['+dropdown_direction2+']='+value_changedvar+';\n';
  return code;
  

};
Blockly.Arduino.Array_var = function() {
  //var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000'
  var dropdown_direction = this.getFieldValue('var_name');
  var dropdown_direction2 = this.getFieldValue('elemento');
  // TODO: Assemble JavaScript into code variable.
  var code= ''+ dropdown_direction +'['+ dropdown_direction2 +']';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};
Blockly.Arduino.The_variable= function() {
  //var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000'
  var dropdown_type = this.getFieldValue('type');
  var text_var_name = Blockly.Arduino.variableDB_.getName(this.getFieldValue('var_name'),Blockly.Variables.NAME_TYPE);
  var value_name = Blockly.Arduino.valueToCode(this, 'val', Blockly.Arduino.ORDER_ATOMIC) || '0';
  // TODO: Assemble JavaScript into code variable.
  Blockly.Arduino.definitions_['define_The_variable'+ text_var_name] = ''+ dropdown_type +  ' '+ text_var_name +'2;';
    var code = ''+ text_var_name +'2 = '+ value_name +';\n';
  return code;
  

};
Blockly.Arduino.The_variable_get= function() {
  //var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000'
  var text_var_name = this.getFieldValue('var_name');
  // TODO: Assemble JavaScript into code variable.
    var code = ''+ text_var_name +'2';
    return [code, Blockly.Arduino.ORDER_ATOMIC];
  

};
Blockly.Arduino.math_number = function() {
  // Numeric value.
  var number1 = this.getFieldValue('NUM');
  // -4.abs() returns -4 in Dart due to strange order of operation choices.
  // -4 is actually an operator and a number.  Reflect this in the order.
  var code = ''+ number1 +'';
       
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};