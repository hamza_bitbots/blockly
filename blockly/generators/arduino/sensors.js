goog.provide('Blockly.Arduino.sensors');

goog.require('Blockly.Arduino');


Blockly.Arduino.distance = function() {
  Blockly.Arduino.definitions_['define_sonar'] ='int sonarnode()\n'+
  '{ radio.ReadingingPipe(address[0]);\n'+
  'radio.startListening();\n'+
  'radio.write( &payload, sizeof(unsigned long) );\n'+
  ' unsigned long time = micros();\n'+
  'delay(20)\n'+
  'int x = payload;\n'+
  'return x; }'
  
  var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000'
 var code='sonarnode()';

  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.Temperature = function() {
  Blockly.Arduino.definitions_['define_Temperature'] = 'int tempnode()\n'+
  '{ radio.WritingingPipe(address[1]);\n'+
  'radio.startListening();\n'+
  'radio.write( &payload, sizeof(unsigned long) );\n'+
  ' unsigned long time = micros();\n'+
  'delay(20)\n'+
  'int x = payload;\n'+
  'return x; }'
  var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000'
 var code='tempnode()';

  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.Knob = function() {
  Blockly.Arduino.definitions_['define_Knob'] = 'int knobnode()\n'+
  '{ radio.WritingingPipe(address[2]);\n'+
  'radio.startListening();\n'+
  'radio.write( &payload, sizeof(unsigned long) );\n'+
  ' unsigned long time = micros();\n'+
  'delay(20)\n'+
  'int x = payload;\n'+
  'return x; }'

  var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000'
 var code='knobnode()';

  return [code, Blockly.Arduino.ORDER_ATOMIC];
};
Blockly.Arduino.Light = function() {
  Blockly.Arduino.definitions_['define_Knob'] = 'int Lightnode()\n'+
  '{ radio.WritingingPipe(address[3]);\n'+
  'radio.startListening();\n'+
  'radio.write( &payload, sizeof(unsigned long) );\n'+
  ' unsigned long time = micros();\n'+
  'delay(20)\n'+
  'int x = payload;\n'+
  'return x; }'
  var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000'
 var code='Lightnode()';

  return [code, Blockly.Arduino.ORDER_ATOMIC];
};