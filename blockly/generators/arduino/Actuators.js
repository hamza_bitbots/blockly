goog.provide('Blockly.Arduino.Actuators');

goog.require('Blockly.Arduino');

Blockly.Arduino.motor = function() {
  //var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000'
  var dropdown_direction = this.getFieldValue('Direction');
  var value_power = Blockly.Arduino.valueToCode(this, 'power', Blockly.Arduino.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  if(dropdown_direction === "forwards")
  {
var code = ' radio.openWritingPipe(address[4]);\n'+'radio.stopListening();\n'+
"payload="+value_power+"+ 1000;\n"+
'radio.write( &payload, sizeof(unsigned long) );\n';
  return code;
  }
  else if(dropdown_direction==="backwards")
  {
    var code = ' radio.openWritingPipe(address[4]);\n'+'radio.stopListening();\n'+
    "payload="+value_power+"+ 2000;\n "+
    'radio.write( &payload, sizeof(unsigned long) );\n';
  return code;
  }
  
};

Blockly.Arduino.servo = function() {
  // TODO: Assemble JavaScript into code variable. 17 ip reserved for 2nd motor
  var mapped;
  var  value_angle = Blockly.Arduino.valueToCode(this, 'angle', Blockly.Arduino.ORDER_ATOMIC);
  var code = ' radio.openWritingPipe(address[5]);\n'+'radio.stopListening();\n'+
  "payload="+value_power+";\n"+
  'radio.write( &payload, sizeof(unsigned long) );\n';
  return code;
   
};

Blockly.Arduino.flash = function() {
  // TODO: Assemble JavaScript into code variable. 17 ip reserved for 2nd motor
  var  value_angle = Blockly.Arduino.valueToCode(this, 'intensity', Blockly.Arduino.ORDER_ATOMIC);
  var code = ' radio.openWritingPipe(address[6]);\n'+'radio.stopListening();\n'+
  "payload="+value_power+"\n"+
  'radio.write( &payload, sizeof(unsigned long) );\n';
  return code;
};

