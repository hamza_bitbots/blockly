goog.provide('Blockly.Arduino.custom');

goog.require('Blockly.Arduino');


/*Blockly.Arduino.setupo = function() {
  Blockly.Arduino.definitions_['define_setup'] = '#include "RF24.h"\n'+'#define CE_PIN 7\n'+'#define CSN_PIN 8\n'+'RF24 radio(CE_PIN, CSN_PIN);\n'+'byte address[11] ='+ '{'+'SonarNode'+""+value_setup+""+','+'TempNode'+','+'KnobNode'+','+'LightNode'+','+'MotorNode'+','+'ServoNode'+','+'FlashNode'+'};\n'+
  'int payload = 0;\n'


  Blockly.Arduino.setups_['setup_custom'] = " Serial.begin(9600);\n"+
  "   radio.begin()\n"+
  "  radio.setAutoAck(1)\n"+
  "   radio.setRetries(15,15)\n";
  var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000'
  var code = '';
  return code;
};*/
Blockly.Arduino.setupo = function() {
  // TODO: Assemble JavaScript into code variable. 17 ip reserved for 2nd motor
  var mapped;
  var  value_setup = Blockly.Arduino.valueToCode(this, 'setup', Blockly.Arduino.ORDER_ATOMIC);
  Blockly.Arduino.definitions_['define_setup'] = '#include "RF24.h"\n'+'#define CE_PIN 7\n'+'#define CSN_PIN 8\n'+'RF24 radio(CE_PIN, CSN_PIN);\n'+'byte address[11] ='+ '{'+'Sonar'+""+value_setup+""+','+'Temp'+""+value_setup+""+','+'Knob'+""+value_setup+""+','+'Light'+""+value_setup+""+','+'Motor'+""+value_setup+""+','+'Servo'+""+value_setup+""+','+'Flash'+""+value_setup+""+'};\n'+
  'int payload = 0;\n'

  Blockly.Arduino.setups_['setup_custom'] = " Serial.begin(9600);\n"+
  "   radio.begin()\n"+
  "  radio.setAutoAck(1)\n"+
  "   radio.setRetries(15,15)\n";
  var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000'
  var code = '';
  return code;
}