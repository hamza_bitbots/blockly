//http://www.seeedstudio.com/wiki/GROVE_System
//http://www.seeedstudio.com/depot/index.php?main_page=advanced_search_result&search_in_description=1&keyword=grovefamily
//support starter bundle example http://www.seeedstudio.com/wiki/GROVE_-_Starter_Kit_V1.1b

/**
 * Visual Blocks Language
 *
 * Copyright 2012 Fred Lin.
 * https://github.com/gasolin/BlocklyDuino
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Helper functions for generating seeeduino grove blocks.
 * @author gasolin@gmail.com (Fred Lin)
 */

goog.provide('Blockly.Arduino.grove');

goog.require('Blockly.Arduino');


Blockly.Arduino.grove_led = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  var dropdown_stat = this.getFieldValue('STAT');
  Blockly.Arduino.setups_['setup_green_led_'+dropdown_pin] = 'pinMode('+dropdown_pin+', OUTPUT);';
  var code = 'digitalWrite('+dropdown_pin+','+dropdown_stat+');\n'
  return code;
};

Blockly.Arduino.grove_button = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  Blockly.Arduino.setups_['setup_button_'+dropdown_pin] = 'pinMode('+dropdown_pin+', INPUT);';
  var code = 'digitalRead('+dropdown_pin+')';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.grove_rotary_angle = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  var code = 'analogRead('+dropdown_pin+')';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.Encoder_sensor = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  Blockly.Arduino.definitions_['define_Encoder-sensor'] = 'int encoderCount2;\n int encoderCount3 ;\nint encode2(){\n encoderCount2++;\n  }\n int encode3(){\n encoderCount3++;\n  }'
  Blockly.Arduino.setups_['setup_Encoder-sensor'+dropdown_pin] = 'pinMode('+dropdown_pin+', INPUT);\n attachInterrupt(digitalPinToInterrupt('+ dropdown_pin+ '), encode'+ dropdown_pin +', CHANGE);';
  var code = 'encoderCount'+dropdown_pin+'';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.Reset_Encoder_sensor = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  var code = 'encoderCount'+dropdown_pin+'=0;';
  return code;
};
/*
int buttonPin = 1;
int buzzerPin = 2;

char notes[] = "cdefgabC "; // a space represents a rest
const int length = sizeof(notes); // the number of notes
int beats[length] = { 1,1,1,1,1,1,1,1,1};

int tempo = 300;

void playTone(int tone, int duration) {
  for (long i = 0; i < duration * 1000L; i += tone * 2) {
    digitalWrite(buzzerPin, HIGH);
    delayMicroseconds(tone);
    digitalWrite(buzzerPin, LOW);
    delayMicroseconds(tone);
  }
}

void playNote(char note, int duration) {
  char names[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C'};
  int tones[] = { 1915, 1700, 1519, 1432, 1275, 1136, 1014, 956 };

  // play the tone corresponding to the note name
  for (int i = 0; i < length; i++) {
    if (names[i] == note) {
      playTone(tones[i], duration);
    }
  }
}

void setup() {
  pinMode(buzzerPin, OUTPUT);
  pinMode(buttonPin,INPUT);
}

void loop() {
  if(digitalRead(buttonPin))
  {
  for (int i = 0; i < length; i++) {
    if (notes[i] == ' ') {
      delay(beats[i] * tempo); // rest
    } else {
      playNote(notes[i], beats[i] * tempo);
    }

    // pause between notes
    delay(tempo / 20);
  }
  }
}
*/
Blockly.Arduino.grove_piezo_buzzer = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  //var dropdown_stat = this.getFieldValue('STAT');
  var value_intensity = Blockly.Arduino.valueToCode(this, 'intensity', Blockly.Arduino.ORDER_ATOMIC) || '0';
  Blockly.Arduino.setups_['setup_piezo_buzzer_'+dropdown_pin] = 'pinMode('+dropdown_pin+', OUTPUT);';
  var code = 'analogWrite('+dropdown_pin+','+value_intensity+');\n'
  return code;
};

Blockly.Arduino.grove_relay = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  var dropdown_stat = this.getFieldValue('STAT');
  Blockly.Arduino.setups_['setup_relay_'+dropdown_pin] = 'pinMode('+dropdown_pin+', OUTPUT);';
  var code = 'digitalWrite('+dropdown_pin+','+dropdown_stat+');\n'
  return code;
};

Blockly.Arduino.grove_temporature_sensor = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  Blockly.Arduino.definitions_['define_temorature_sensor'] = '#include <dht.h>\n dht DHT;\n int tempSensor(){\n DHT.read11(A0);\n int x = DHT.temperature;\n return x;}'
   
  /*
  a=analogRead(0);
  resistance=(float)(1023-a)*10000/a;
  temperature=1/(log(resistance/10000)/B+1/298.15)-273.15;
  */
  var code = 'tempSensor()';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

/*
#include <SerialLCD.h>
#include <SoftwareSerial.h> //this is a must
SerialLCD slcd(11,12);//this is a must, assign soft serial pins

void setup()
{
  slcd.begin();// set up :
}

void loop()
{
  slcd.backlight();// Turn on the backlight: //noBacklight
  slcd.setCursor(0,0); // set the cursor to (0,0):
  slcd.print("  Seeed Studio"); // Print a message to the LCD.
  slcd.setCursor(0,1); //line 2
  slcd.print("   Starter kit   ");
  delay(5000);
  //slcd.scrollDisplayLeft();//scrollDisplayRight/autoscroll/
  //slcd.clear();
  //Power/noPower
}
*/

var _get_next_pin = function(dropdown_pin) {
  var NextPIN = dropdown_pin;
  if(parseInt(NextPIN)){
    NextPIN = parseInt(dropdown_pin)+1;
  } else {
    NextPIN = 'A'+(parseInt(NextPIN.slice(1,NextPIN.length))+1);
  }
  //check if NextPIN in bound
  var pinlen = profile.default.digital.length;
  var notExist=true;
  for(var i=0;i<pinlen;i++){
    if(profile.default.digital[i][1] == NextPIN){
      notExist=false;
    }
  }
  if(notExist){
    alert("Grove Sensor needs PIN#+1 port, current setting is out of bound.");
    return null;
  } else {
    return NextPIN;
  }
}

Blockly.Arduino.grove_serial_lcd_print = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  var text = Blockly.Arduino.valueToCode(this, 'TEXT',
      Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
  var text2 = Blockly.Arduino.valueToCode(this, 'TEXT2',
      Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
  var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000';
  /*if(text.length>16||text2.length>16){
      alert("string is too long");
  }*/
  Blockly.Arduino.definitions_['define_seriallcd'] = '#include <SerialLCD.h>\n';
  Blockly.Arduino.definitions_['define_softwareserial'] = '#include <SoftwareSerial.h>\n';
  //generate PIN#+1 port
  var NextPIN = _get_next_pin(dropdown_pin);

  Blockly.Arduino.definitions_['var_lcd_'+dropdown_pin] = 'SerialLCD slcd_'+dropdown_pin+'('+dropdown_pin+','+NextPIN+');\n';

  Blockly.Arduino.setups_['setup_lcd_'+dropdown_pin] = 'slcd_'+dropdown_pin+'.begin();\n';
  var code = 'slcd_'+dropdown_pin+'.backlight();\n';
  code    += 'slcd_'+dropdown_pin+'.setCursor(0,0);\n';
  code    += 'slcd_'+dropdown_pin+'.print('+text+');\n';//text.replace(new RegExp('\'',"gm"),'')
  code    += 'slcd_'+dropdown_pin+'.setCursor(0,1);\n';
  code    += 'slcd_'+dropdown_pin+'.print('+text2+');\n';
  code    += 'delay('+delay_time+');\n';
  return code;
};

Blockly.Arduino.grove_serial_lcd_power = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  var dropdown_stat = this.getFieldValue('STAT');

  Blockly.Arduino.definitions_['define_seriallcd'] = '#include <SerialLCD.h>\n';
  Blockly.Arduino.definitions_['define_softwareserial'] = '#include <SoftwareSerial.h>\n';
  //generate PIN#+1 port
  var NextPIN = _get_next_pin(dropdown_pin);

  Blockly.Arduino.definitions_['var_lcd'+dropdown_pin] = 'SerialLCD slcd_'+dropdown_pin+'('+dropdown_pin+','+NextPIN+');\n';
  var code = 'slcd_'+dropdown_pin;
  if(dropdown_stat==="ON"){
    code += '.Power();\n';
  } else {
    code += '.noPower();\n';
  }
  return code;
};

Blockly.Arduino.grove_serial_lcd_effect = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  var dropdown_stat = this.getFieldValue('STAT');

  Blockly.Arduino.definitions_['define_seriallcd'] = '#include <SerialLCD.h>\n';
  Blockly.Arduino.definitions_['define_softwareserial'] = '#include <SoftwareSerial.h>\n';
  //generate PIN#+1 port
  var NextPIN = _get_next_pin(dropdown_pin);

  Blockly.Arduino.definitions_['var_lcd'+dropdown_pin] = 'SerialLCD slcd_'+dropdown_pin+'('+dropdown_pin+','+NextPIN+');\n';
  var code = 'slcd_'+dropdown_pin;
  if(dropdown_stat==="LEFT"){
    code += '.scrollDisplayLeft();\n';
  } else if(dropdown_stat==="RIGHT"){
    code += '.scrollDisplayRight();\n';
  } else {
    code += '.autoscroll();\n';
  }
  return code;
};

Blockly.Arduino.grove_sound_sensor = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  /*Blockly.Arduino.definitions_['define_sound-sensor'] = 'int ChECkEr'+ dropdown_pin +';\n  int sound_sensor(){\n'+'while(ChECkEr'+ dropdown_pin +'==0){\n'+'for(int i=0;i<5;i++){int x=analogRead('+ dropdown_pin +');\n ChECkEr'+ dropdown_pin +'= ChECkEr'+ dropdown_pin +'+ x;}\n'+
   'ChECkEr'+ dropdown_pin +'=ChECkEr'+ dropdown_pin +'/5;}\n int z=analogRead('+ dropdown_pin +');\n if(z>ChECkEr'+ dropdown_pin +'+3){\n' +' z=1;}\n else{\n z=0;} return z;} \n';*/
   Blockly.Arduino.setups_['setup_sound_'+dropdown_pin] = 'pinMode('+ dropdown_pin +',INPUT);\n'
  var code = 'digitalRead('+ dropdown_pin +')';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.Flame_sensor = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  Blockly.Arduino.definitions_['define_Flame-sensor'] = 'int Flame_sensor(){\n'+'int y;\n int x=analogRead('+ dropdown_pin +');\n'+
   'if(x>=900){\n' +' y=1;}\n else if(x>=300 && x<900) {\n y=2;}\n else if(x>=0 && x<200){\n y=3; } \n return y;} \n';
   Blockly.Arduino.setups_['define_flame-sensor'+dropdown_pin] = 'pinMode('+ dropdown_pin +',INPUT);\n'
  var code = 'Flame_sensor()';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.grove_line_finder = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  Blockly.Arduino.setups_['setup_input_'+dropdown_pin] = 'pinMode('+dropdown_pin+', INPUT);';
  var code = 'digitalRead('+dropdown_pin+')';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.grove_ultrasonic_ranger = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  var dropdown_pin1 = this.getFieldValue('PIN2');
  //var dropdown_unit = this.getFieldValue('UNIT');
  Blockly.Arduino.definitions_['define_ultrasonic'] = 'int sonari(int x, int y){\n'+'int sonardist,sonardist1;\n'+'digitalWrite(x, LOW);\n'+ 'delayMicroseconds(2);\n'+'digitalWrite(x , HIGH);\n'+' delayMicroseconds(10);\n'+'digitalWrite(x, LOW);\n'+
  'sonardist= pulseIn(y , HIGH);\n'+ 'sonardist=sonardist / 58;\n'+'if(sonardist>1){return sonardist;}}\n';
  Blockly.Arduino.setups_['setup_input_'+ dropdown_pin + dropdown_pin1 ] = 'pinMode('+ dropdown_pin +', OUTPUT);\n'+'pinMode('+ dropdown_pin1 +', INPUT);\n';
  var code = 'sonari('+ dropdown_pin +', '+ dropdown_pin1 +')\n';
 
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.motor_shield = function() {
  var dropdown_pin = this.getFieldValue('pin1');
  var dropdown_pin2 = this.getFieldValue('pin2');
  var dropdown_pin3 = this.getFieldValue('direction');
  //var speed = 127;//Blockly.Arduino.valueToCode(this, 'SPEED', Blockly.Arduino.ORDER_ATOMIC) || '127';
  var value_speed = Blockly.Arduino.valueToCode(this, 'motor', Blockly.Arduino.ORDER_ATOMIC) || '0';
  Blockly.Arduino.setups_['setup_motor'+ dropdown_pin + dropdown_pin2] =
  "  pinMode("+dropdown_pin+",OUTPUT);//I2\n"+
  "  pinMode("+dropdown_pin2+",OUTPUT);//speedPinA\n"
  var code = "";
  if(dropdown_pin3==="forward"){
    Blockly.Arduino.definitions_['define_forward'] = "void forward(int x, int y, int z)\n"+
"{\n"+
"  analogWrite(x ,z);//input a simulation value to set the speed\n"+
"  digitalWrite(y ,LOW);//turn DC Motor B move anticlockwise\n"+
     
"}\n";
    code="forward("+ dropdown_pin +", " + dropdown_pin2 + ","+ value_speed +");\n";

  } else if (dropdown_pin3==="backward") {
    Blockly.Arduino.definitions_['define_backward'] = "void backward(int x, int y, int z)\n"+
"{\n"+
     "  analogWrite(y, z);//input a simulation value to set the speed\n"+
     "  digitalWrite(x,LOW);//turn DC Motor B move anticlockwise\n"+
"}\n\n";
    code="backward("+ dropdown_pin +", " + dropdown_pin2 + ", "+ value_speed +");\n";
  }/* else if (dropdown_direction==="left") {
    Blockly.Arduino.definitions_['define_left'] = "void left()\n"+
"{\n"+
     "  analogWrite(9,"+speed+");//input a simulation value to set the speed\n"+
     "  analogWrite(10,"+speed+");\n"+
     "  digitalWrite(13,HIGH);//turn DC Motor B move clockwise\n"+
     "  digitalWrite(12,LOW);\n"+
     "  digitalWrite(11,HIGH);//turn DC Motor A move clockwise\n"+
     "  digitalWrite(8,LOW);\n"+
"}\n\n";
    code="left();\n";
  } else if (dropdown_direction==="backward"){
    Blockly.Arduino.definitions_['define_backward'] = "void backward()\n"+
"{\n"+
     "  analogWrite(9,"+speed+");//input a simulation value to set the speed\n"+
     "  analogWrite(10,"+speed+");\n"+
     "  digitalWrite(13,LOW);//turn DC Motor B move anticlockwise\n"+
     "  digitalWrite(12,HIGH);\n"+
     "  digitalWrite(11,HIGH);//turn DC Motor A move clockwise\n"+
     "  digitalWrite(8,LOW);\n"+
"}\n\n";
    code="backward();\n";
  } else if (dropdown_direction==="stop"){
    Blockly.Arduino.definitions_['define_stop'] = "void stop()\n"+
"{\n"+
     "digitalWrite(9,LOW);// Unenble the pin, to stop the motor. this should be done to avid damaging the motor.\n"+
     "digitalWrite(10,LOW);\n"+
     "delay(1000);\n"+
"}\n\n"
    code="stop();\n";
  }*/
  return code;
};
Blockly.Arduino.Laser_beam =  function() {
  var dropdown_pin = this.getFieldValue('PIN');
  var value_power = Blockly.Arduino.valueToCode(this, 'laserPower', Blockly.Arduino.ORDER_ATOMIC) || '0';
  var value_delay = Blockly.Arduino.valueToCode(this, 'laserDelay', Blockly.Arduino.ORDER_ATOMIC) || '0';
  Blockly.Arduino.definitions_['define_Laser_beam'] = "void Laser_beam(int x , int y)\n"+
  "{\n"+
  "  analogWrite("+ dropdown_pin +" , y);\n"+
  "  delay(x);\n"+
  "analogWrite("+ dropdown_pin +" ,0);\n"+ 
  "  delay(x);\n"+
  "}\n";
  var code = 'Laser_beam('+ value_delay +', '+ value_power +');';
  return code;
};

Blockly.Arduino.grove_thumb_joystick =  function() {
  var dropdown_pin = this.getFieldValue('PIN');
  var dropdown_axis = this.getFieldValue('AXIS');
  var stickPIN = "0"
  if(dropdown_axis==="y"){
    stickPIN = _get_next_pin(dropdown_pin);
  } else {
    stickPIN = dropdown_pin
  }
  var code = 'analogRead('+stickPIN+')';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

function hexToR(h) {return parseInt((cutHex(h)).substring(0,2),16)}
function hexToG(h) {return parseInt((cutHex(h)).substring(2,4),16)}
function hexToB(h) {return parseInt((cutHex(h)).substring(4,6),16)}
function cutHex(h) {return (h.charAt(0)=="#") ? h.substring(1,7):h}

Blockly.Arduino.Touch_sensor = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  Blockly.Arduino.setups_['setup_input_'+dropdown_pin] = 'pinMode('+dropdown_pin+', INPUT);';
  var code= 'digitalRead('+ dropdown_pin +')';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.grove_bluetooth_slave = function() {
  var dropdown_pin= this.getFieldValue('mode');
  var dropdown_pin1= this.getFieldValue('Type');
  var value_data = Blockly.Arduino.valueToCode(this, 'send_data', Blockly.Arduino.ORDER_ATOMIC) || '0';
  var code='';
  Blockly.Arduino.definitions_['Blutooth_initialize'] = 'String BTString;\nint BTint;\nint servo_pos1;\nint servo_pos2;\n'+
  'void recv_int(){int ReadBatLevel=analogRead(A7);\n int sent_i=map(ReadBatLevel, 0,1024 , 0, 255);\nsent_i=sent_i+5000;\nSerial.write(lowByte(sent_i));\nSerial.write(highByte(sent_i));\ndelay(100);'+"\n while (Serial.available()>=2){\n servo_pos1 = Serial.read();\nservo_pos2 = Serial.read();\nBTint= (servo_pos2 * 256) + servo_pos1;}\n}"+
  'void recv_String(){\nint ReadBatLevel=analogRead(A7);\n int sent_i=map(ReadBatLevel, 0,1024 , 0, 255);\nsent_i=sent_i+5000;\nSerial.write(lowByte(sent_i));\nSerial.write(highByte(sent_i));\ndelay(100);\n'+'BTString = "";'+"for(int x=0;x<20000;x++){\n while (Serial.available()){\nchar c=Serial.read();\n BTString +=c;}}}\n"+
  'void send_int(int x){\nint sent_ii=x+1000;\nSerial.write(lowByte(sent_ii));\nSerial.write(highByte(sent_ii));\ndelay(100);}\n';
 
  
  Blockly.Arduino.setups_['bluetooth_setup' ] = 'Serial.begin(9600);\n'+'pinMode(A7, INPUT);\n';
  if (dropdown_pin=='recieve' && dropdown_pin1=='int')
  {
   code ='recv_int();\n';
 
  }
 
  else if(dropdown_pin=='recieve' && dropdown_pin1=='String' )
 {
  code = 'recv_String();\n';
 }
 
  else if(dropdown_pin=='send' && dropdown_pin1=='int')
  {
   code= 'send_int('+ value_data +');\n';
 
  }
  else if(dropdown_pin=='send' && dropdown_pin1=='String')
  {
   
   code= '\nBTString = '+ value_data +';\n'+'Serial.print(BTString)\n';
 
  }
   return code;
 };

Blockly.Arduino.Rugpod_setup = function() {
  Blockly.Arduino.definitions_['Rugpod_initialize'] = '#include <Servo.h>\n'+
  'Servo one; Servo two; Servo three;\n';
  var code= '';
  return code;
};
Blockly.Arduino.Move_Forward = function() {
  Blockly.Arduino.definitions_['Rugpod_forward']=  'void forward(){\n'+'int t= 4;\n'+ 'one.attach(13); two.attach(11); three.attach(12);\n'+
  'for(int i=90, j=90; i<=180||j>=0; i++,j--)\n'+
  '{three.write(i); two.write(j); delay(t); }\n'+
  'for(int j=0; j<=60; j++)\n'+
  '{ two.write(j); delay(t); }\n'+
  'for(int i=180,j=60; i>=90|| j<=90; i--,j++)\n'+
  '{ three.write(j); two.write(j);  delay(t);\nif(j>90)\n{j--;}}\n one.detach(); two.detach(); three.detach();}\n';
  var code = "forward();";
  return code;
};

Blockly.Arduino.Move_Left = function() {
  Blockly.Arduino.definitions_['Rugpod_left'] = 
  'void left(){\n'+'int t= 5;\n'+ 'one.attach(13); two.attach(11); three.attach(12);\n'+

  '   for(int i=90,j=90; i<=120|| j>=40; i++,j--)\n'+
  '{   two.write(j); three.write(i);     delay(t+4);  }\n'+

  'for(int i=90; i<=140; i++)\n'+
  ' {   one.write(i);     delay(t);  }\n'+

  'for(int i=120, j=40; i>=90|| j<=90; i--,j++)\n'+
  '{   two.write(j); three.write(i);     delay(t+4);}\n'+

  '  for(int i=140; i>=90; i--)\n'+
  ' {   one.write(i);     delay(t+4);  }\n'+

  ' one.detach(); two.detach(); three.detach();\n}';
  var code = "left();";
  return code;
};
Blockly.Arduino.Move_Right = function() {
  Blockly.Arduino.definitions_['Rugpod_right'] =   'void right(){\n'+'int t= 5;\n'+ 'one.attach(13); two.attach(11); three.attach(12);\n'+

  'for(int i=90,j=90; i<=120|| j>=40; i++,j--)\n'+
  '{   two.write(j); three.write(i);     delay(t+4);  }\n'+

  '   for(int i=90; i>=40; i--)\n'+
  '{   one.write(i);     delay(t);  }\n'+

  ' for(int i=120, j=40; i>=90|| j<=90; i--,j++)\n'+
  ' {   two.write(j); three.write(i);     delay(t+4); }\n'+

  '  for(int i=40; i<=90; i++)\n'+
  '{  one.write(i);      delay(t+4);  }\n'+

  ' one.detach(); two.detach(); three.detach();}\n';
  var code = "right();";
  return code;
};
Blockly.Arduino.Fall_Down = function() {
  Blockly.Arduino.definitions_['Rugpod_down'] =  'void flex(){\n'+ 'int t= 8;\n'+ 'one.attach(13); two.attach(11); three.attach(12);\n'+
  'for(int i=90; i<=135; i++)\n'+
  '{three.write(i); two.write(i); one.write(i); delay(t); }\n'+
  'for(int i=135; i>=45; i--)\n'+
  '{ three.write(i);  one.write(i); delay(t); }\n'+
  'for(int i=45; i<=90; i++)\n'+
  '{ three.write(i);  one.write(i); delay(t); }\n one.detach(); two.detach(); three.detach();}\n';
  var code = "flex();";
  return code;
};
Blockly.Arduino.Get_up = function() {
  Blockly.Arduino.definitions_['Rugpod_getup'] =  'void Get_Up(){\n'+ 'int t= 3;\n'+ 'one.attach(13); two.attach(11); three.attach(12);\n'+
  'for(int i=90,j=90; i<=180|| j>=0; i++,j--)\n'+
'{   two.write(j); three.write(i);     delay(t+4);  }\n'+
 ' for(int i=90; i>=40; i--)\n'+
'{   one.write(i);     delay(t);  }\n'+
'for(int i=180; i>=90;i--)\n'+
'{  three.write(i);     delay(t+4);}\n'+
 'for(int j=0;j<=90;j++)\n'+
'{  two.write(j);      delay(t+4);  }\n'+
 'for(int i=40; i<=90; i++)\n'+
 '{   one.write(i);     delay(t+4);  }\n'+
  'one.detach(); two.detach(); three.detach();}\n';
  var code = "Get_Up();";
  return code;
};
Blockly.Arduino.Blutooth_Rugpod = function() {
  var dropdown_pin =  this.getFieldValue('Type');
  var code = 'BT'+dropdown_pin+'';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
 
};

Blockly.Arduino.Rugpod_forward = function() {
  var code = '"'+'g'+'"';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};
Blockly.Arduino.Rugpod_right = function() {
  var code = '"'+'r'+'"';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};
Blockly.Arduino.Rugpod_left = function() {
  var code = '"'+'l'+'"';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};
Blockly.Arduino.Rugpod_falldown = function() {
  var code = '"'+'f'+'"';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};
Blockly.Arduino.A_button = function() {
  var code = '"'+'aR'+'"';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};
Blockly.Arduino.B_button = function() {
  var code = '"'+'bG'+'"';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.Rugpod_distance = function() {

  //var dropdown_unit = this.getFieldValue('UNIT');
  Blockly.Arduino.definitions_['define_ultrasonic'] = 'int sonari(int x, int y){\n'+'int sonardist,sonardist1;\n'+'digitalWrite(x, LOW);\n'+ 'delayMicroseconds(2);\n'+'digitalWrite(x , HIGH);\n'+' delayMicroseconds(10);\n'+'digitalWrite(x, LOW);\n'+
  'sonardist= pulseIn(y , HIGH);\n'+ 'sonardist=sonardist / 58;\n'+'if (sonardist>0 && sonardist<=40){\n'+'sonardist1=sonardist;}\n else {\n sonardist1= 40; }'+'return sonardist1;}\n';
  Blockly.Arduino.setups_['setup_input_'] = 'pinMode(8, OUTPUT);\n'+'pinMode(9, INPUT);\n';
  var code = 'sonari(8, 7)\n';
 
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};




Blockly.Arduino.Lizzy_setup = function() {
  Blockly.Arduino.definitions_['Lizzy_initialize'] = '#include <Servo.h>\n'+
  'Servo front; Servo torso; Servo hind;\n';
  var code=  '';
  return code;
};


Blockly.Arduino.Move_ForwardLizzy = function() {
  Blockly.Arduino.definitions_['Lizzy_forward'] =   'void ForwardLizzy(){\n'+'int t= 4;\n'+ 'hind.attach(13); torso.attach(11); front.attach(12);\n'+
  'for (int i=90,j=90; i<= 140|| j>=40; i++,j--)\n'+
  '  { front.write(i); torso.write(110); hind.write(j); delay(t); } \n'+
  '  for (int i=140,j=40; i>=40|| j<=140; i--,j++)  \n'+
  '  { front.write(i); torso.write(65); hind.write(j); delay(t); }  \n'+
  '  for (int i=40, j=140; i<= 90|| j>=90; i++,j--)\n'+
  '{ front.write(i); torso.write(110); hind.write(j); delay(t); }\n  delay(1);\n hind.detach(); torso.detach(); front.detach();}\n';
  var code = "ForwardLizzy();";
  return code;
};

Blockly.Arduino.Move_LeftLizzy = function() {
  Blockly.Arduino.definitions_['Lizzy_left'] = 'void LeftLizzy(){\n'+'int t= 4;\n'+ 'hind.attach(13); torso.attach(11); front.attach(12);\n'+

  'for (int i=90,j=90; i>= 30|| j<=100;i--,j++)\n'+
  '{ front.write(i); torso.write(i-25); hind.write(j);  delay(t);}\n'+

  '  for (int i=30,j=100; i<=100|| j>=80; i++,j--) \n'+
  '{  front.write(i);  torso.write(80);  hind.write(j);  delay(t); }\n'+

  'for (int i=100,j=80; i>= 90||j<=90; i--, j++)'+
  '   { front.write(i); torso.write(80);  hind.write(j);  delay(t); }\n'+
'delay(1);\n'+
  ' hind.detach(); torso.detach(); front.detach();}\n';
  var code = "LeftLizzy();";
  return code;
};
Blockly.Arduino.Move_RightLizzy = function() {
  Blockly.Arduino.definitions_['Lizzy_right'] = 
  'void RightLizzy(){\n'+'int t= 4;\n'+ 'hind.attach(13); torso.attach(11); front.attach(12);\n'+

  ' for (int i=90,j=90; i<= 150|| j>=80;i++,j--)\n'+
  '{front.write(i); torso.write(i); hind.write(j);    delay(t);}\n'+

  '   for (int i=150,j=80; i>=80|| j<=100; i--,j++)\n'+
  '  {  front.write(i);  torso.write(80);  hind.write(j);  delay(t); }\n'+

  'for (int i=80,j=100; i<= 90||j>=90; i++, j--)\n'+
  '{ front.write(i); torso.write(80);  hind.write(j);  delay(t); }\n'+

  ' delay(1);\n hind.detach(); torso.detach(); front.detach();}\n';
  var code = "RightLizzy();";
  return code;
};
Blockly.Arduino.Move_BackwardLizzy = function() {
  Blockly.Arduino.definitions_['Lizzy_backward'] =  'void BackwardLizzy(){\n'+ 'int t= 4;\n'+ 'hind.attach(13); torso.attach(11); front.attach(12);\n'+
  'for (int i=90,j=90; i<= 140|| j>=40; i++,j--)\n'+
  '{ front.write(i); torso.write(55); hind.write(j); delay(t); }\n'+
  ' for (int i=140,j=40; i>=40|| j<=140; i--,j++)\n'+
  '  { front.write(i); torso.write(105); hind.write(j); delay(t); }\n'+
  '  for (int i=40, j=140; i<= 90|| j>=90; i++,j--)\n'+
  '{ front.write(i); torso.write(55); hind.write(j); delay(t); }\n  delay(1);\n hind.detach(); torso.detach(); front.detach();}\n';
  var code = "BackwardLizzy();";
  return code;
};


Blockly.Arduino.CustomMovementsRugpod = function() {
  var dropdown_pin = this.getFieldValue('servos');
  var value_from = Blockly.Arduino.valueToCode(this, 'from', Blockly.Arduino.ORDER_ATOMIC);
  var value_to = Blockly.Arduino.valueToCode(this, 'to', Blockly.Arduino.ORDER_ATOMIC);
  var value_delay = Blockly.Arduino.valueToCode(this, 'delay', Blockly.Arduino.ORDER_ATOMIC) || '1';
  var pin='';
  var code = '';
  if (dropdown_pin=='one.')
  {pin='13';}
  else if(dropdown_pin=='two.')
  {pin='11';}
  else if(dropdown_pin=='three.')
  {pin='12';}
  
  Blockly.Arduino.definitions_['define_defaultmov'+ dropdown_pin] ="int custom_mov"+ pin +"(int x , int y, int z){\nif(x>y) {\n"+ dropdown_pin +"attach("+ pin +");\nfor( int countR= x; countR >= y; countR--  )\n "+
  "{ "+ dropdown_pin +"write(countR); \n delay(z);}\none.detach(); two.detach(); three.detach();\n} else if(y>x) { "+ dropdown_pin +"attach("+ pin +");\nfor( int countR= x; countR <= y; countR++  )\n "+
  "{ "+ dropdown_pin +"write(countR); \n delay(z);}\n one.detach(); two.detach(); three.detach();\n} else { "+ dropdown_pin +"attach("+ pin +");\n "+ dropdown_pin +"write(x);\n delay(z); }}"
    code= "custom_mov"+ pin +"( "+ value_from +", "+ value_to +" , "+ value_delay +" );\n";
  
  return code;
};

Blockly.Arduino.RugPod_calibration = function() {
  var code = " one.attach(13); two.attach(11); three.attach(12);\n one.write(90);\ntwo.write(75);\nthree.write(48);\n";
  return code;
};

Blockly.Arduino.CustomMovementsLizzy = function() {
  var dropdown_pin = this.getFieldValue('servos');
  var value_from = Blockly.Arduino.valueToCode(this, 'from', Blockly.Arduino.ORDER_ATOMIC);
  var value_to = Blockly.Arduino.valueToCode(this, 'to', Blockly.Arduino.ORDER_ATOMIC);
  var value_delay = Blockly.Arduino.valueToCode(this, 'delay', Blockly.Arduino.ORDER_ATOMIC) || '1';
  var pin='';
  var code = '';
  if (dropdown_pin=='front.')
  {pin='13';}
  else if(dropdown_pin=='torso.')
  {pin='11';}
  else if(dropdown_pin=='hind.')
  {pin='12';}
  Blockly.Arduino.definitions_['define_defaultmov'+ dropdown_pin] ="int custom_mov"+ pin +"(int x , int y, int z){\nif(x>y) {\n"+ dropdown_pin +"attach("+ pin +");\nfor( int countR= x; countR >= y; countR--  )\n "+
  "{ "+ dropdown_pin +"write(countR); \n delay(z);}\none.detach(); two.detach(); three.detach();\n} else if(y>x) { "+ dropdown_pin +"attach("+ pin +");\nfor( int countR= x; countR <= y; countR++  )\n "+
  "{ "+ dropdown_pin +"write(countR); \n delay(z);}\n one.detach(); two.detach(); three.detach();\n} else { "+ dropdown_pin +"attach("+ pin +");\n "+ dropdown_pin +"write(x);\n delay(z); }}"
    code= "custom_mov"+ pin +"( "+ value_from +", "+ value_to +" , "+ value_delay +" );\n";
  return code;
};


Blockly.Arduino.Lizzy_calibration = function() {
  var code = " hind.attach(13); torso.attach(11); front.attach(12);\n hind.write(90);\ntorso.write(90);\nfront.write(90);\n";
  return code;
};

Blockly.Arduino.shake = function() {
  var code = '"'+'S'+'"';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.Martian_setup = function() {
  Blockly.Arduino.definitions_['Martian_initialize'] = '#include <Servo.h>\n'+
  'Servo RT; Servo LT; Servo RA; Servo LA;\n';
  var code=  '';
  return code;
};

Blockly.Arduino.Martian_Forward = function() {
  Blockly.Arduino.definitions_['Martian_forward'] =  'void MartianForward(){\n'+ 'int t=6;RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);for(int i=100; i<=129; i++){LT.write(i);delay(t);  }for(int i=90; i<=120; i++){RT.write(i);delay(t+4);  }\n'+
  'for(int i=120; i>=60; i--){RA.write(i);LA.write(i);delay(t);  }for(int i=129; i>=90; i--){if(i>=100){LT.write(i);}if(i<=120){RT.write(i);}delay(t+4);  }\n'+
  '  for(int i=90; i<=129; i++){RT.write(180-i);delay(t);  }for(int i=80; i<=110; i++){LT.write(180-i);delay(t+4);  }\n'+
  ' for(int i=126; i>=60; i--){RA.write(180-i);LA.write(180-i); delay(t);  }for(int i=129; i>=80; i--){ if(i>=90){ RT.write(180-i);}if(i<=120){LT.write(180-i);}delay(t+4); }\n'+
  'RT.detach(); LT.detach(); RA.detach(); LA.detach();}\n';
  var code = "MartianForward();";
  return code;
};

Blockly.Arduino.Martian_Left = function() {
  Blockly.Arduino.definitions_['Martian_left'] =  'void MartianLeft(){\n'+ 'int t= 4;RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);RT.write(90); RA.write(90); LT.write(100); LA.write(90); for(int i=90; i>=50; i--){RT.write(i);  delay(t); }\n'+
  'for(int i=100; i>=62; i--){ LT.write(i);delay(t);}for(int i=90; i<=125; i++){ LA.write(i); delay(t+2);  } \n'+
  'for(int i=50; i<=90; i++){RT.write(i); delay(t);  }for(int i=62; i<=100; i++){LT.write(i);delay(t+4);   }for(int i=125; i>=90; i--){LA.write(i); delay(t+2);  }RT.detach(); LT.detach(); RA.detach(); LA.detach();}\n';
  var code = "MartianLeft();";
  return code;
};

Blockly.Arduino.Martian_Right = function() {
  Blockly.Arduino.definitions_['Martian_right'] =  'void MartianRight(){\n'+ ' int t= 4; RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);RT.write(90); RA.write(90); LT.write(100); LA.write(90);for(int i=100; i<=133; i++){LT.write(i);  delay(t);  }\n'+
  'for(int i=90; i<=126; i++){ RT.write(i);delay(t); }for(int i=90; i>=55; i--){RA.write(i); delay(t+2);  }\n'+
  'for(int i=133; i>=100; i--){LT.write(i); delay(t);  }for(int i=126; i>=90; i--){RT.write(i);delay(t+4);   }\n'+
  ' for(int i=55; i<=90; i++){RA.write(i); delay(t+2);  }RT.detach(); LT.detach(); RA.detach(); LA.detach();}\n';
  var code = "MartianRight();";
  return code;
};

Blockly.Arduino.Martian_DabR = function() {
  Blockly.Arduino.definitions_['Martian_dabr'] =  'void MartianDabR(){\n'+ ' int t=4; RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);for(int i=90; i>=52; i--){RT.write(i);delay(t);}\n'+
  'for(int i=100; i>=62; i--){LT.write(i);delay(t+6); }for(int i=0;i<=4;i++){RT.write(100);delay(100);  RT.write(60); delay(100); }\n'+
  'for(int i=62; i<=100; i++){LT.write(i);delay(t+6);  }for(int i=60; i<=90; i++){RT.write(i);delay(t); }RT.detach(); LT.detach(); RA.detach(); LA.detach();}\n';
  var code = "MartianDabR();";
  return code;
};
Blockly.Arduino.Martian_Sit = function() {
  Blockly.Arduino.definitions_['Martian_sit'] =  'void MartianSit(){\n'+ 'RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);for(int i=90; i>=0; i--){RT.write(180-i);LT.write(i);delay(12);  }RT.detach(); LT.detach(); RA.detach(); LA.detach();}';
  var code = "MartianSit();";
  return code;
};

Blockly.Arduino.Martian_Stand = function() {
  Blockly.Arduino.definitions_['Martian_stand'] =  'void MartianStand(){\n'+ '  RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);for(int i=0; i<=100; i++){LT.write(i);if(i<=90){RT.write(180-i);}delay(12);  }RT.detach(); LT.detach(); RA.detach(); LA.detach();}\n';
  var code = "MartianStand();";
  return code;
};

Blockly.Arduino.Martian_Dance12 = function() {
  Blockly.Arduino.definitions_['Martian_dance12'] =  'void MartianDance12(){\n'+ ' RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);int t=6;for(int x=0; x<=1; x++){for(int i=100, j=139; i<=139 && j>=90; i++,j--){ LT.write(i); RT.write(90-(j-90));delay(t); }\n'+
  ' for(int i=139, j=90; i>=100 && j<=139; i--,j++){ LT.write(i); RT.write(180-j); delay(t); }}RT.detach(); LT.detach(); RA.detach(); LA.detach();}';
  var code = "MartianDance12();";
  return code;
};

Blockly.Arduino.Martian_Dance22 = function() {
  Blockly.Arduino.definitions_['Martian_dance22'] =  'void MartianDance22(){\n'+ ' RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);int t=7;for(int i=90, j=139; i<=139 && j>=90; i++,j--){LT.write(i); RT.write(90-(j-90));if(j<=110){RA.write(90-(j-90));}if(i<=110){LA.write(i);} delay(t); }\n'+
  ' for(int i=139, j=90; i>=90 && j<=139; i--,j++){LT.write(i); RT.write(180-j);if(j<=110){ RA.write(180-j);} if(i<=110){LA.write(i);}delay(t);}RT.detach(); LT.detach(); RA.detach(); LA.detach();}';
  var code = "MartianDance22();";
  return code;
};

Blockly.Arduino.Martian_DabL = function() {
  Blockly.Arduino.definitions_['Martian_dabl'] =  'void MartianDabL(){\n'+ ' int t=4;RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);for(int i=100; i>=52; i--){LT.write(180-i);delay(t);  }for(int i=90; i>=56; i--){RT.write(180-i);delay(t+6); }\n'+
  'for(int i=0;i<=4;i++){LT.write(100);delay(100);LT.write(60); delay(100);}for(int i=124; i>=90; i--){RT.write(i);delay(t+6);  }\n'+
  'for(int i=60; i<=100; i++){LT.write(i);delay(t);  }RT.detach(); LT.detach(); RA.detach(); LA.detach();}\n';
  var code = "MartianDabL();";
  return code;
};

Blockly.Arduino.Martian_Dance32 = function() {
  Blockly.Arduino.definitions_['Martian_dance32'] =  'void MartianDance32(){\n'+ ' int t=5;RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);for(int i=90; i>=40; i--){RT.write(i);LT.write(180-i);delay (t);}for(int j=0; j<=4; j++){for(int i=90; i<=124; i++){RA.write(i);LA.write(i); delay (t);}\n'+
  'for(int i=124; i>=90; i--){RA.write(i);LA.write(i);delay (t);}for(int i=90; i>=56; i--){RA.write(i);LA.write(i);delay (t);}\n'+
  'for(int i=56; i<=90; i++){RA.write(i);LA.write(i);delay (t);} }for(int i=40; i<=90; i++){RT.write(i);LT.write(180-i);delay (t);}RT.detach(); LT.detach(); RA.detach(); LA.detach();}\n';
  var code = "MartianDance32();";
  return code;
};

Blockly.Arduino.Martian_WaveR = function() {
  Blockly.Arduino.definitions_['Martian_waver'] =  'void WaveR(){\n'+ ' int t=5;RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);for(int i=90; i>=40; i--){RT.write(i);delay(t);  }for(int i=40; i<=90; i++){RT.write(i);LT.write(90+(i-40));delay(t);}\n'+
  'for(int i=140; i>=90; i--){LT.write(i);delay(t); }RT.detach(); LT.detach(); RA.detach(); LA.detach();}';
  var code = "WaveR();";
  return code;
};

Blockly.Arduino.Martian_WaveL = function() {
  Blockly.Arduino.definitions_['Martian_wavel'] =  'void WaveL(){\n'+ ' int t=5;RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);for(int i=90; i>=40; i--){LT.write(180-i);delay(t);  }for(int i=40; i<=90; i++){LT.write(180-i);RT.write(180-(90+(i-40)));delay(t);  }\n'+
  '  for(int i=140; i>=90; i--){RT.write(180-i);delay(t);  }RT.detach(); LT.detach(); RA.detach(); LA.detach();}';
  var code = "WaveL();";
  return code;
};

Blockly.Arduino.Martian_Custom = function() {
  var dropdown_pin = this.getFieldValue('Option');
  var value_servo = Blockly.Arduino.valueToCode(this, 'NAME', Blockly.Arduino.ORDER_ATOMIC) || '90';
 
  var code = 'RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);'+
  ''+ dropdown_pin +'.write('+ value_servo +');\n';
  return code;
};

Blockly.Arduino.Martian_calibration = function() {
  Blockly.Arduino.definitions_['Martian_calibration'] =  'void MartianCallibrate(){\n'+ ' RT.attach(12); RA.attach(11); LT.attach(2);LA.attach(13);RT.write(90); LT.write(100); LA.write(90); RA.write(90);}';
  var code = "MartianCallibrate();";
  return code;
};
