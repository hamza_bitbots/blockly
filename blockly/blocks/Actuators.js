goog.provide('Blockly.Blocks.Actuators');

goog.require('Blockly.Blocks');

Blockly.Blocks['motor'] = {
    init: function() {
      this.appendValueInput("power")
          .setCheck("Number")
          .appendField("motor")
          .appendField(new Blockly.FieldDropdown([["Forwards","forwards"], ["Backwards","backwards"]]), "Direction");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(0);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };

  Blockly.Blocks['servo'] = {
    init: function() {
      this.appendValueInput("angle")
          .setCheck("Number")
          .appendField("servo");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(0);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };

  Blockly.Blocks['flash'] = {
    init: function() {
      this.appendValueInput("intensity")
          .setCheck("Number")
          .appendField("flash");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(0);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };