goog.provide('Blockly.Blocks.sensors');

goog.require('Blockly.Blocks');

Blockly.Blocks['distance'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("distance sensor");
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['Temperature'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Temperature sensor");
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['Knob'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Knob");
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['Light'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Light");
    this.setOutput(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

