//http://www.seeedstudio.com/wiki/GROVE_System
//http://www.seeedstudio.com/depot/index.php?main_page=advanced_search_result&search_in_description=1&keyword=grovefamily
//support starter bundle example http://www.seeedstudio.com/wiki/GROVE_-_Starter_Kit_V1.1b

/**
 * @license
 * Visual Blocks Editor
 *
 * Copyright 2012 Fred Lin.
 * https://github.com/gasolin/BlocklyDuino
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Helper functions for generating seeeduino grove blocks.
 * @author gasolin@gmail.com (Fred Lin)
 */

goog.provide('Blockly.Blocks.grove');

goog.require('Blockly.Blocks');


Blockly.Blocks['grove_led'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#LED',
  init: function() {
    this.setColour(120);
    this.appendDummyInput()
        .appendField("LED")
        .appendField(new Blockly.FieldImage("https://www.kitronik.co.uk/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/3/5/3524_large_white_5mm_ultra_bright_led.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN")
        .appendField("stat")
        .appendField(new Blockly.FieldDropdown([["HIGH", "HIGH"], ["LOW", "LOW"]]), "STAT");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('green LED');
  }
};

Blockly.Blocks['grove_button'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#Button',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Button")
        .appendField(new Blockly.FieldImage("https://statics3.seeedstudio.com/images/product/bgpushb1.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN");
    this.setOutput(true, 'Boolean');
    this.setTooltip('Basic digital input');
  }
};

Blockly.Blocks['grove_rotary_angle'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#Potentiometer',
  init: function() {
    this.setColour(10);
    this.appendDummyInput()
        .appendField("Rotary Angle")
        .appendField(new Blockly.FieldImage("https://statics3.seeedstudio.com/images/product/GroveRotaryP.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.analog), "PIN");
    this.setOutput(true, 'Number');
    this.setTooltip('Analog output between 0 and Vcc');
  }
};

Blockly.Blocks['Encoder_sensor'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#Tilt_switch',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Encoder sensor")
        .appendField(new Blockly.FieldImage("http://klinikrobot.com/components/com_virtuemart/shop_image/product/Encoder_Infrared_566b09067fa9f.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN");
    this.setOutput(true, 'Boolean');
    this.setTooltip('When the switch is level it is open, and when tilted, the switch closes.');
  }
};

Blockly.Blocks['Reset_Encoder_sensor'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#Tilt_switch',
  init: function() {
    this.appendDummyInput()
        .appendField("Reset Encoder sensor")
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN");
     
       this.setPreviousStatement(true, null);
       this.setNextStatement(true, null);
        
        
    this.setColour(190);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['grove_piezo_buzzer'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/GROVE_-_Starter_Kit_V1.1b#Grove_.E2.80.93_Buzzer',
  init: function() {
  
    this.setColour(120);
    this.appendDummyInput()
        .appendField("Piezo Buzzer")
        .appendField(new Blockly.FieldImage("https://tkkrlab.nl/w/images/thumb/2/23/KY-006_Small_passive_buzzer_module_Sku_138322.jpg/400px-KY-006_Small_passive_buzzer_module_Sku_138322.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN")
        .appendField("stat")
        this.appendValueInput("intensity")
        .setCheck("Number");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Emit a tone when the output is high');
  }
};

Blockly.Blocks['grove_relay'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Relay',
  init: function() {
    this.setColour(120);
    this.appendDummyInput()
        .appendField("Relay")
        .appendField(new Blockly.FieldImage("https://statics3.seeedstudio.com/images/1030200051.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN")
        .appendField("stat")
        .appendField(new Blockly.FieldDropdown([["HIGH", "HIGH"], ["LOW", "LOW"]]), "STAT");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('capable of switching a much higher voltages and currents. The maximum voltage and current that can be controlled by this module upto 250V at 10 amps.');
  }
};

Blockly.Blocks['grove_temporature_sensor'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Project_Seven_-_Temperature',
  init: function() {
    this.setColour(10);
    this.appendDummyInput()
        .appendField("Temporature Sensor")
        .appendField(new Blockly.FieldImage("https://statics3.seeedstudio.com/images/101020015%201.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.analog), "PIN")
    this.setOutput(true, 'Number');
    this.setTooltip('return number of ambient temperature in ℃');
  }
};

Blockly.Blocks['grove_serial_lcd_print'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#Serial_LCD',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Serial LCD")
        .appendField(new Blockly.FieldImage("https://statics3.seeedstudio.com/images/product/Serial%20LCD.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN");
    this.appendValueInput("TEXT", 'String')
        .setCheck('String')
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("print line1");
    this.appendValueInput("TEXT2", 'String')
        .setCheck('String')
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("print line2")
    this.appendValueInput("DELAY_TIME", 'Number')
        .setCheck('Number')
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Delay");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('print text on an 16 character by 2 line LCD.');
  }
};

//grove lcd power on/off
Blockly.Blocks['grove_serial_lcd_power'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#LED',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Serial LCD")
        .appendField(new Blockly.FieldImage("https://statics3.seeedstudio.com/images/product/Serial%20LCD.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Power")
        .appendField(new Blsckly.FieldDropdown([["ON", "ON"], ["OFF", "OFF"]]), "STAT");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Turn LCD power on/off');
  }
};

//scroll left/right/no scroll/blink/noblink
Blockly.Blocks['grove_serial_lcd_effect'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#LED',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Serial LCD")
        .appendField(new Blockly.FieldImage("https://statics3.seeedstudio.com/images/product/Serial%20LCD.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Effect")
        .appendField(new Blockly.FieldDropdown([["Scroll Left", "LEFT"], ["Scroll Right", "RIGHT"], ["Scroll Auto", "AUTO"]]), "STAT");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Turn LCD power on/off');
  }
};

Blockly.Blocks['grove_sound_sensor'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Sound_Sensor',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Sound Sensor")
        .appendField(new Blockly.FieldImage("https://img.dxcdn.com/productimages/sku_150778_1.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN")
    this.setOutput(true, 'Number');
    this.setTooltip('Detect the sound strength of the environment gives 0 as output');
  }
};

Blockly.Blocks['Flame_sensor'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_PIR_Motion_Sensor',
  init: function() {
    this.setColour(10);
    this.appendDummyInput()
        .appendField("Flame_sensor")
        .appendField(new Blockly.FieldImage("https://cdn-global-hk.hobbyking.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/legacy/catalog/84745_high.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.analog), "PIN")
    this.setOutput(true, 'Number');
    this.setTooltip('Detects fire! gives 1 as output if no fire as the fire nears the output goes from 2 to 3');
  }
};

Blockly.Blocks['grove_line_finder'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Line_Finder',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Line Finder")
        .appendField(new Blockly.FieldImage("https://diygeeks.org/wp-content/uploads/2016/08/ir-sensor.jpg", 64, 64))
	      .appendField("PIN#")
	      .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN");
    this.setOutput(true, 'Number');
    this.setTooltip('Output digital signal so the robot can reliably follow a black line on a white background');
  }
};

Blockly.Blocks['grove_ultrasonic_ranger'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Ultrasonic_Ranger',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
	      .appendField("Ultrasonic Ranger")
        .appendField(new Blockly.FieldImage("https://statics3.seeedstudio.com/seeed/img/2016-09/kIyY21sbC6ct7JYzCWf1mAPs.jpg", 64, 64))
	      .appendField("PIN# Trig")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN")
        .appendField("PIN# Echo")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN2")
    this.setOutput(true, 'Number');
    this.setTooltip('distance measurement in cm');
  }
};

Blockly.Blocks['motor_shield'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Motor_Shield',
  init: function() {
    this.appendValueInput("motor")
        .setCheck("Number")
        .appendField("Motor")
        .appendField(new Blockly.FieldImage("https://www.makerlab-electronics.com/my_uploads/2017/06/DC-motor.jpg", 64, 64))
        .appendField("Pin 1")
        .appendField(new Blockly.FieldDropdown([["3","3"], ["5","5"], ["6","6"], ["9","9"], ["10","10"], ["11","11"]]), "pin1")
        .appendField("Pin 2")
        .appendField(new Blockly.FieldDropdown([["3","3"], ["5","5"], ["6","6"], ["9","9"], ["10","10"], ["11","11"]]), "pin2")
        .appendField("Direction")
        .appendField(new Blockly.FieldDropdown([["Forward","forward"], ["Backward","backward"]]), "direction");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(120);
 this.setTooltip("Control a motor");
 this.setHelpUrl("");
 
  }
};
Blockly.Blocks['Laser_beam'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Motor_Shield',
  init: function() {
    this.appendValueInput("laserPower")
        .setCheck("Number")
        .appendField("Laser beam")
        .appendField("PIN# ")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN")
        .appendField("Power")
        this.appendValueInput("laserDelay")
        .setCheck("Number")
        .appendField(new Blockly.FieldImage("https://aws.robu.in/wp-content/uploads/2017/09/laser-diode-module-5mw-650nm-india-500x500.jpg", 64, 64))
        .appendField("Frequency")
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(120);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['grove_thumb_joystick'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Thumb_Joystick',
  init: function() {
    this.setColour(10);
    this.appendDummyInput()
	.appendField("Thumb Joystick")
        .appendField(new Blockly.FieldImage("https://statics3.seeedstudio.com/images/product/bgjoy1.jpg", 64, 64))
	.appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.analog), "PIN")
        .appendField("axis")
        .appendField(new Blockly.FieldDropdown([["x", "x"],  ["y", "y"]]), "AXIS");
    this.setOutput(true, 'Number');
this.setTooltip('output two analog values(200~800) representing two directions');
  }
};

Blockly.Blocks['Touch_sensor'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=Twig_-_Chainable_RGB_LED',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
  .appendField("Touch sensor")
        .appendField(new Blockly.FieldImage("https://cdn-global-hk.hobbyking.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/legacy/catalog/37886.jpg", 64, 64))
  .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.digital), "PIN")
   /* this.appendDummyInput("COLOR0")
        .setAlign(Blockly.ALIGN_RIGHT)*/
        /*.appendField("Color 1")
        .appendField(new Blockly.FieldColour("#00ff00"), "RGB0");
    this.setMutator(new Blockly.Mutator(['grove_rgb_led_item']));*/
    this.setOutput(true, "Number");
    this.setTooltip('senses touch gives 1 as output on touch');
    /*this.itemCount_ = 1;*/
}
};
  

Blockly.Blocks['grove_rgb_led_container'] = {
  // Container.
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Container");
    this.appendStatementInput('STACK');
    this.setTooltip("Add, remove items to reconfigure this chain");
    this.contextMenu = false;
  }
};

Blockly.Blocks['grove_bluetooth_slave'] = {
  init: function() {
    this.appendValueInput("send_data")
        .setCheck(["Number", "String"])
        .appendField("Bluetooth")
        .appendField(new Blockly.FieldImage("https://5.imimg.com/data5/AK/TP/MY-9380557/bluetooth-module-hc-05-500x500.jpg", 100, 100, "Bluetooth Module"))
        .appendField(new Blockly.FieldDropdown([["Send","send"], ["Recieve","recieve"]]), "mode")
        .appendField(new Blockly.FieldDropdown([["Number","int"], ["String","String"]]), "Type");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
 this.setTooltip("Bluetooth module");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Rugpod_setup'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("INITIAL SETUP for Rugpod");
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Move_Forward'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("RugPod_Forward.gif", 64, 64))
        .appendField("Move forward");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Move_Left'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("RugPod_left.gif", 64, 64))
        .appendField("Move Left");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Move_Right'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("RugPod_right.gif", 64, 64))
        .appendField("Move Right");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Fall_Down'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("RugPod_fall.gif", 64, 64))
        .appendField("Fall Down");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['Get_up'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Get up");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['Blutooth_Rugpod'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("blutooth input")
    .appendField(new Blockly.FieldDropdown([["Number","int"], ["String","String"]]), "Type");
    this.setOutput(true, null);
    this.setColour(210);
 this.setTooltip("");
 this.setHelpUrl("");
  },
};
Blockly.Blocks['Rugpod_forward'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("up.png", 64, 64))
        .appendField("Up condition");
    this.setOutput(true, "String");
    this.setColour(195);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['Rugpod_right'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("right.png", 64, 64))
        .appendField("Right Condition");
    this.setOutput(true, "String");
    this.setColour(195);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['Rugpod_left'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("left.png", 64, 64))
        .appendField("Left Condition");
    this.setOutput(true, "String");
    this.setColour(195);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Rugpod_falldown'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("down.png", 64, 64))
        .appendField("Down Condition");
    this.setOutput(true, "String");
    this.setColour(195);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Rugpod_distance'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Distance Sensor");
    this.setOutput(true, null);
    this.setColour(300);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};





Blockly.Blocks['Lizzy_setup'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("INITIAL SETUP for Lizzy");
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Move_ForwardLizzy'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Move forward");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Move_LeftLizzy'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Move Left");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Move_RightLizzy'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Move Right");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Move_BackwardLizzy'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Go Back");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};



Blockly.Blocks['CustomMovementsRugpod'] = {
  init: function() {
    this.appendValueInput("from")
        .setCheck("Number")
        .appendField("Custom Movements")
        .appendField(new Blockly.FieldDropdown([["Front Hinge","one."], ["Back","two."], ["Foot","three."]]), "servos")
        .appendField("From");
    this.appendValueInput("to")
        .setCheck("Number")
        .appendField("To");
    this.appendValueInput("delay")
        .setCheck("Number")
        .appendField("Delay");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['RugPod_calibration'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("RugPod Calibration");
        this.setPreviousStatement(true, null);
    this.setColour(75);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};


Blockly.Blocks['CustomMovementsLizzy'] = {
  init: function() {
    this.appendValueInput("from")
        .setCheck("Number")
        .appendField("Custom Movements")
        .appendField(new Blockly.FieldDropdown([["Front","front."], ["Torso","torso."], ["Hind","hind."]]), "servos")
        .appendField("From");
    this.appendValueInput("to")
        .setCheck("Number")
        .appendField("To");
    this.appendValueInput("delay")
        .setCheck("Number")
        .appendField("Delay");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Lizzy_calibration'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Lizzy Calibration");
        this.setPreviousStatement(true, null);
    this.setColour(75);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['shake'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Phone shake condition");
    this.setOutput(true, "Boolean");
    this.setColour(195);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Martian_Forward'] = {
  init: function() {
    this.appendDummyInput()
    //.appendField(new Blockly.FieldImage("RugPod_Forward.gif", 64, 64))
        .appendField("Go forward");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['Martian_Left'] = {
  init: function() {
    this.appendDummyInput()
    //.appendField(new Blockly.FieldImage("RugPod_Forward.gif", 64, 64))
        .appendField("Go Left");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['Martian_Right'] = {
  init: function() {
    this.appendDummyInput()
    //.appendField(new Blockly.FieldImage("RugPod_Forward.gif", 64, 64))
        .appendField("Go Right");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['Martian_DabR'] = {
  init: function() {
    this.appendDummyInput()
    //.appendField(new Blockly.FieldImage("RugPod_Forward.gif", 64, 64))
        .appendField("Shake Right");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['Martian_DabL'] = {
  init: function() {
    this.appendDummyInput()
    //.appendField(new Blockly.FieldImage("RugPod_Forward.gif", 64, 64))
        .appendField("Shake left");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Martian_Sit'] = {
  init: function() {
    this.appendDummyInput()
    //.appendField(new Blockly.FieldImage("RugPod_Forward.gif", 64, 64))
        .appendField("Sit");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Martian_Stand'] = {
  init: function() {
    this.appendDummyInput()
    //.appendField(new Blockly.FieldImage("RugPod_Forward.gif", 64, 64))
        .appendField("Stand");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Martian_Dance12'] = {
  init: function() {
    this.appendDummyInput()
    //.appendField(new Blockly.FieldImage("RugPod_Forward.gif", 64, 64))
        .appendField("Dance One");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Martian_Dance22'] = {
  init: function() {
    this.appendDummyInput()
    //.appendField(new Blockly.FieldImage("RugPod_Forward.gif", 64, 64))
        .appendField("Dance Two");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['Martian_Dance32'] = {
  init: function() {
    this.appendDummyInput()
    //.appendField(new Blockly.FieldImage("RugPod_Forward.gif", 64, 64))
        .appendField("Dance three");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Martian_WaveR'] = {
  init: function() {
    this.appendDummyInput()
    //.appendField(new Blockly.FieldImage("RugPod_Forward.gif", 64, 64))
        .appendField("Wave right");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Martian_setup'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("INITIAL SETUP for Martian");
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Martian_WaveL'] = {
  init: function() {
    this.appendDummyInput()
    //.appendField(new Blockly.FieldImage("RugPod_Forward.gif", 64, 64))
        .appendField("Wave left");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Martian_Custom'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck("Number")
        .appendField("Custom Movement")
        //.appendField(new Blockly.FieldImage("", 64, 64, "*"))
        .appendField(new Blockly.FieldDropdown([["Right Leg","RA"], ["Right Foot","RT"], ["Left Leg","LA"], ["Left Foot","LT"]]), "Option");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    this.setColour(290);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
Blockly.Blocks['A_button'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("red.png", 64, 64))
        .appendField("A Button");
    this.setOutput(true, "String");
    this.setColour(195);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['B_button'] = {
  init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("green.png", 64, 64))
        .appendField("B Button");
    this.setOutput(true, "String");
    this.setColour(195);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['Martian_calibration'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Martian Calibration");
        this.setPreviousStatement(true, null);
    this.setColour(75);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
//http://www.seeedstudio.com/wiki/File:Twig-Temp%26Humi.jpg
//http://www.seeedstudio.com/wiki/Grove-_Temperature_and_Humidity_Sensor

//http://www.seeedstudio.com/wiki/Grove_-_125KHz_RFID_Reader

/*
void setup()
{
	pinMode( 3 , OUTPUT);
	pinMode( 1 , INPUT);
}

void loop()
{
	if (digitalRead( 1))
	{
		digitalWrite( 3 , HIGH);
	}
	else
	{
		digitalWrite( 1 , LOW);
	}
}
*/
