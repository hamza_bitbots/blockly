goog.provide('Blockly.Blocks.Arrays');

goog.require('Blockly.Blocks');

Blockly.Blocks['Array_variable'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["Char","char"], ["string","String"], ["Number","int"],["float","float"]]), "type");
    this.appendValueInput("item1")
        .setCheck(null)     
        .appendField("Array")
        .appendField(new Blockly.FieldTextInput("name"), "var_name");
    this.appendValueInput("item2")
        .setCheck(null);
    this.appendValueInput("item3")
        .setCheck(null);
    this.appendValueInput("item4")
        .setCheck(null);
    this.appendValueInput("item5")
        .setCheck(null);
        
    this.setColour(210);
 this.setTooltip("An array that can hold 5 element");
 this.setHelpUrl("");
  },
 
  };
  Blockly.Blocks['Array_pointer'] = {
    init: function() {
      this.appendValueInput("change_element")
          .setCheck("Number")
          .appendField(new Blockly.FieldTextInput("name"), "var_name")
          .appendField(new Blockly.FieldDropdown([["0","0"], ["1","1"], ["2","2"], ["3","3"], ["4","4"]]), "NAME2");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(210);
   this.setTooltip("Set any element in the an Array");
   this.setHelpUrl("");
    }
    };

    Blockly.Blocks['Array_var'] = {
        init: function() {
            this.appendDummyInput()
            .appendField(new Blockly.FieldTextInput("name"), "var_name")
                .appendField(new Blockly.FieldDropdown([["0","0"], ["1","1"], ["2","2"], ["3","3"], ["4","4"]]), "elemento");
            this.setOutput(true, null);
            this.setColour(230);
         this.setTooltip("get the value of an element from an Array");
         this.setHelpUrl("");
        }
        };
        Blockly.Blocks['The_variable'] = {
            init: function() {
                this.appendValueInput("val")
                    .setCheck(null)
                    .appendField("set variable")
                    .appendField(new Blockly.FieldDropdown([["integer","int"], ["string","String"], ["float","float"],["char","char"] ]), "type")
                    .appendField(new Blockly.FieldTextInput("item"), "var_name");
                this.setPreviousStatement(true, null);
                this.setNextStatement(true, null);
                this.setColour(345);
             this.setTooltip("A variable to hold content");
             this.setHelpUrl("");
              },
          
            };
            Blockly.Blocks['The_variable_get'] = {
                init: function() {
                    this.appendDummyInput()
                        .appendField("get variable")
                        .appendField(new Blockly.FieldTextInput("item"), "var_name");
                    this.setOutput(true, null);
                    this.setColour(345);
                 this.setTooltip("get the content stored in a variable");
                 this.setHelpUrl("");
                  },
              
                };
                Blockly.Blocks['math_number'] = {
                    /**
                     * Block for numeric value.
                     */
                    init: function() {
                      this.setHelpUrl(Blockly.Msg.MATH_NUMBER_HELPURL);
                      this.setColour(Blockly.Blocks.math.HUE);
                      this.appendDummyInput()
                          .appendField(new Blockly.FieldTextInput('0'),'NUM');
                      this.setOutput(true, "Number");
                      this.setTooltip(Blockly.Msg.MATH_NUMBER_TOOLTIP);
                    }
                  };