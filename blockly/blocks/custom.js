goog.provide('Blockly.Blocks.custom');

goog.require('Blockly.Blocks');

/*Blockly.Blocks['setupo'] = {
  helpUrl: 'http://arduino.cc/en/Reference/delay',
  init: function() {
    this.appendDummyInput()
        .appendField("setup");
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};*/
Blockly.Blocks['setupo'] = {
  init: function() {
    this.appendValueInput("setup")
        .setCheck(null)
        .appendField("setup");
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};